/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 2;  /* border pixel of windows */
static const unsigned int snap      = 32; /* snap pixel */
static const unsigned int gappih    = 5;  /* horiz inner gap between windows */
static const unsigned int gappiv    = 5;  /* vert inner gap between windows */
static const unsigned int gappoh    = 5;  /* horiz outer gap between windows and screen edge */
static const unsigned int gappov    = 5;  /* vert outer gap between windows and screen edge */
static const int smartgaps          = 0;  /* 1 means no outer gap when there is only one window */
static const int showbar            = 1;  /* 0 means no bar */
static const int topbar             = 1;  /* 0 means bottom bar */
/* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systraypinning = 1;
/* 0: systray in the right corner, >0: systray on left of status text */
static const unsigned int systrayonleft  = 0;
/* systray spacing */
static const unsigned int systrayspacing = 2;
/* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int systraypinningfailfirst = 1;
/* 0 means no systray */
static const int showsystray             = 1;

static const char *fonts[]          = { "Iosevka Slab:size=9" };
static const char dmenufont[]       = "Iosevka Slab:size=9";
static const char col_gray1[]       = "#222222"; // Background color
static const char col_gray2[]       = "#444444"; // Inactive window border color
static const char col_gray3[]       = "#bbbbbb"; // Font color
static const char col_gray4[]       = "#eeeeee"; // Current tag and current window font color
static const char col_cyan[]        = "#005577"; // Top bar second color (blue) and active window border color

static const char col_pnight1[]     = "#2e3440"; // Polar Night
static const char col_pnight2[]     = "#3b4252"; // Polar Night
static const char col_pnight3[]     = "#434c5e"; // Polar Night
static const char col_pnight4[]     = "#4c566a"; // Polar Night

static const char col_au_red[]      = "#bf616a"; // Nordtheme Aurora
static const char col_au_orange[]   = "#d08770"; // Nordtheme Aurora
static const char col_au_yellow[]   = "#ebcb8b"; // Nordtheme Aurora
static const char col_au_green[]    = "#a3be8c"; // Nordtheme Aurora
static const char col_au_lila[]     = "#b48ead"; // Nordtheme Aurora

static const char col_frost1[]      = "#8fbcbb"; // Nordtheme Frost
static const char col_frost2[]      = "#88c0d0"; // Nordtheme Frost
static const char col_frost3[]      = "#81a1c1"; // Nordtheme Frost
static const char col_frost4[]      = "#5e81ac"; // Nordtheme Frost

// static const char col_gray1[]       = "#1d2021"; // PROBAR COLORES
// static const char col_gray2[]       = "#8ec07f"; // PROBAR COLORES
// static const char col_gray3[]       = "#a89984"; // PROBAR COLORES
// static const char col_gray4[]       = "#689d6a"; // PROBAR COLORES
// static const char col_cyan[]        = "#ebdbb2"; // PROBAR COLORES

// Creo que es un color verde
// static const char col_gray1[]       = "#282828"; // PROBAR COLORES
// static const char col_gray2[]       = "#444444"; // PROBAR COLORES
// static const char col_gray3[]       = "#a89984"; // PROBAR COLORES
// static const char col_gray4[]       = "#eeeeee"; // PROBAR COLORES
// static const char col_cyan[]        = "#4d6e4e"; // PROBAR COLORES

static const char *colors[][3]      = {
	/*               fg         bg          border     */
	[SchemeNorm] = { col_gray3, col_gray1,  col_gray2  },
	[SchemeSel]  = { col_gray4, col_frost4, col_frost4 },
};

/* tagging */
static const char *tags[] = { "Λ", "Σ", "Ω", "Θ", "Π", "Φ", "Γ", "Ψ", "Ξ" };
// Α α, Β β, Γ γ, Δ δ, Ε ε, Ζ ζ, Η η, Θ θ, Ι ι, Κ κ, Λ λ, Μ μ, Ν ν, Ξ ξ, Ο ο, Π π, Ρ ρ, Σ σ/ς, Τ τ, Υ υ, Φ φ, Χ χ, Ψ ψ, and Ω ω

/* commands */
static char            dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char      *dmenucmd[] = { "dmenu_run", NULL};
static const char       *termcmd[] = { "st", NULL };
static const char scratchpadname[] = "scratchpad";
static const char *scratchpadcmd[] = { "st", "-t", scratchpadname, "-g", "120x34", NULL };
static const char    *browsercmd[] = { "firefox", NULL };
static const char    *spotifycmd[] = { "spotify", NULL };
static const char    *dolphincmd[] = { "dolphin", NULL };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor */
	//{ "Gimp",     NULL,       NULL,       0,            1,           -1 },
	//{ "Firefox",  NULL,       NULL,       1 << 8,       0,           0 },
	{ "firefox",  NULL,       NULL,       0,            0,           0 },
	{ "Spotify",  NULL,       NULL,       0,            0,           1 },
	{ "dolphin",  NULL,       NULL,       0,            0,           2 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* key definitions */
#define ALTKEY Mod1Mask
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

// Botones de audio
static const char *vold[] = { "amixer", "-q", "set", "Master", "5%-", "unmute", NULL };
static const char *volu[] = { "amixer", "-q", "set", "Master", "5%+", "unmute", NULL };
// static const char *mute[] = { "amixer", "-D", "pulse", "set", "Master", "1+", "toggle", NULL };
// static const char *mic[]  = { "amixer", "-q", "set", "Mic", "toggle", NULL }; // REVISAR, NO FUNCIONA
static const char *play[] = { "playerctl", "play-pause", NULL };
static const char *next[] = { "playerctl", "next", NULL };
static const char *prev[] = { "playerctl", "previous", NULL };
static const char *bklu[] = { "xbacklight", "-steps", "1", "-time", "0", "-inc", "5", NULL };
static const char *bkld[] = { "xbacklight", "-steps", "1", "-time", "0", "-dec", "5", NULL };

#include "X11/XF86keysym.h" // Para los botones de audio

static Key keys[] = {
	/* modifier                     key                         function        argument */
	{ MODKEY,                       XK_space,                   spawn,          {.v = dmenucmd } },
	{ MODKEY,                       XK_Return,                  spawn,          {.v = termcmd } },
	{ MODKEY|ALTKEY,                XK_Return,                  togglescratch,  {.v = scratchpadcmd } },
	{ MODKEY,                       XK_w,                       spawn,          {.v = browsercmd } },
	{ MODKEY,                       XK_x,                       spawn,          {.v = spotifycmd } },
	{ MODKEY,                       XK_e,                       spawn,          SHCMD("emacs") },
//	{ MODKEY,                       XK_e,                       spawn,          SHCMD("emacsclient -n -c -a 'emacs'") },
	{ MODKEY,                       XK_c,                       spawn,          {.v = dolphincmd } },
	{ 0,                            XF86XK_AudioLowerVolume,    spawn,          {.v = vold } },
	{ 0,                            XF86XK_AudioRaiseVolume,    spawn,          {.v = volu } },
//	{ 0,                            XF86XK_AudioMute,           spawn,          {.v = mute } },
	{ 0,                            XF86XK_AudioMute,           spawn,          SHCMD("amixer -D pulse set Master 1+ toggle") },
//	{ 0,                            XF86XK_AudioMicMute,        spawn,          {.v = mic } }, // REVISAR, NO FUNCIONA
	{ 0,                            XF86XK_AudioPlay,           spawn,          {.v = play } },
	{ 0,                            XF86XK_AudioNext,           spawn,          {.v = next } },
	{ 0,                            XF86XK_AudioPrev,           spawn,          {.v = prev } },
//	{ 0,                            XF86XK_AudioStop,           spawn,          {.v = stop } }, // NO TENGO BOTON STOP
	{ 0,                            XF86XK_MonBrightnessUp,     spawn,          {.v = bklu } },
	{ 0,                            XF86XK_MonBrightnessDown,   spawn,          {.v = bkld } },
	{ MODKEY,                       XK_b,                       togglebar,      {0} },
	{ MODKEY,                       XK_j,                       focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,                       focusstack,     {.i = -1 } },
//	{ MODKEY,                       XK_i,                       incnmaster,     {.i = +1 } },
//	{ MODKEY,                       XK_d,                       incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,                       setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,                       setmfact,       {.f = +0.05} },
	{ MODKEY|ShiftMask,             XK_Return,                  zoom,           {0} },

    /*****************************************| Gaps |**************************************/
	{ MODKEY|Mod1Mask,              XK_k,                       incrgaps,       {.i = +1 } },
	{ MODKEY|Mod1Mask,              XK_j,                       incrgaps,       {.i = -1 } },
	{ MODKEY|Mod1Mask,              XK_l,                       incrigaps,      {.i = +1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_l,                       incrigaps,      {.i = -1 } },
	{ MODKEY|Mod1Mask,              XK_h,                       incrogaps,      {.i = +1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_h,                       incrogaps,      {.i = -1 } },
	{ MODKEY|Mod1Mask,              XK_u,                       incrihgaps,     {.i = +1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_u,                       incrihgaps,     {.i = -1 } },
	{ MODKEY|Mod1Mask,              XK_i,                       incrivgaps,     {.i = +1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_i,                       incrivgaps,     {.i = -1 } },
	{ MODKEY|Mod1Mask,              XK_o,                       incrohgaps,     {.i = +1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_o,                       incrohgaps,     {.i = -1 } },
	{ MODKEY|Mod1Mask,              XK_p,                       incrovgaps,     {.i = +1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_p,                       incrovgaps,     {.i = -1 } },
	{ MODKEY|Mod1Mask,              XK_a,                       togglegaps,     {0} },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_a,                       defaultgaps,    {0} },

	{ MODKEY,                       XK_Tab,                     view,           {0} },
	{ MODKEY,                       XK_q,                       killclient,     {0} },
	{ MODKEY,                       XK_t,                       setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,                       setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,                       setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_g,                       setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_g,                       togglefloating, {0} },
	{ MODKEY,                       XK_0,                       view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,                       tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,                   focusmon,       {.i = +1 } },
	{ MODKEY,                       XK_period,                  focusmon,       {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_comma,                   tagmon,         {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_period,                  tagmon,         {.i = -1 } },
	TAGKEYS(                        XK_1,                                       0)
	TAGKEYS(                        XK_2,                                       1)
	TAGKEYS(                        XK_3,                                       2)
	TAGKEYS(                        XK_4,                                       3)
	TAGKEYS(                        XK_5,                                       4)
	TAGKEYS(                        XK_6,                                       5)
	TAGKEYS(                        XK_7,                                       6)
	TAGKEYS(                        XK_8,                                       7)
	TAGKEYS(                        XK_9,                                       8)
	//{ MODKEY|ShiftMask,             XK_q,                       quit,           {0} },
	{ MODKEY|ShiftMask,             XK_q,                       spawn,          SHCMD("./Scripts/powersystem.sh") },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

